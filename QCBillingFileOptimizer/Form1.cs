﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QCBillingFileOptimizer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            RemoveZeroCost.Enabled = false;
            label3.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Create OpenFileDialog 
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".csv";
            dlg.Filter = "Csv Files (*.csv)|*.csv";


            // Display OpenFileDialog by calling ShowDialog method 
            dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
             // Open document 
                string filename = dlg.FileName;
                SourceUrl.Text = filename;
            RemoveZeroCost.Enabled = true;


        }

        private void SourceUrl_TextChanged(object sender, EventArgs e)
        {

        }


        private void RemoveZeroCost_Click(object sender, EventArgs e)
        {
            
            List<string> listofrecords = new List<string>();
            string tempFile = Path.GetTempFileName();
            string ZerocosttempFile = Path.GetTempFileName();
            var newfilename =   Path.GetDirectoryName(SourceUrl.Text)+"\\"+Path.GetFileName(SourceUrl.Text) +"_zero_cost_removed_"+ DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";
            var ZeroCostnewfilename = Path.GetDirectoryName(SourceUrl.Text) + "\\" + Path.GetFileName(SourceUrl.Text) + "_zero_cost_only_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";
            String contents = String.Empty;
            label4.Text = "Reading the file";
            int linenumber = File.ReadAllLines(SourceUrl.Text).Length;
            label3.Text = (linenumber-2).ToString();
            progressBar1.Maximum = linenumber;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            using (var sr = new StreamReader(SourceUrl.Text))
            using (var sw = new StreamWriter(tempFile))
            {
                var zerosw = new StreamWriter(ZerocosttempFile);
                string line;
                label4.Text = "Removing 0 $cost";

                while ((line = sr.ReadLine()) != null)
                {
                    progressBar1.PerformStep();

                    if (progressBar1.Value == 1)
                    {
                        sw.WriteLine(line);
                        zerosw.WriteLine(line);
                    }
                    else
                    if(progressBar1.Value < linenumber && !checkLastTotalLine(line))
                    {
                        if (!checkZeroCost(line))
                        {
                            sw.WriteLine(line);
                        }
                        else
                        {
                            zerosw.WriteLine(line);
                        }
                    }
                   

                }
                zerosw.Dispose();
            }


            File.Move(tempFile, newfilename);
            File.Move(ZerocosttempFile, ZeroCostnewfilename);


            //if (File.Exists(SourceUrl.Text) && SourceUrl.Text != "")
            //{
            //    listofrecords = File.ReadAllLines(SourceUrl.Text)
            //                               .Skip(1)
            //                               .Where(v=> checkZeroCost(v)== false)
            //                               .ToList();
            //    values = listofrecords.Count();

            //    label3.Text =   values.ToString();

            //}



            label4.Text = "Complete, File is ready in same directory.";


        }

        public bool checkZeroCost(string line)
        {
           
            var match = Regex.Match(line, @"(^.*?.,0\.0{6}.*\b)", RegexOptions.IgnoreCase);

            if (match.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public bool checkLastTotalLine(string line)
        {

            var match = Regex.Match(line, @"(^[,]{1,}Total)", RegexOptions.IgnoreCase);

            if (match.Success)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private void label2_Click(object sender, EventArgs e)
            {

            }
        }
}
